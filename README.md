# Junior Front-end Developer Test
Thank you for applying for a Junior Front-end Developer position at M&C Saatchi. We've prepared a short test for you to complete. Completing the test shouldn't take you more than 2 hours and it will help us assess if you are a good match for the role.

**Important:** Please don't exceed 2 hours time limit. You are welcome to share your code with us even if the test is not finished (it's not the ultimate goal here).

## Test details

Please create a functional landing page using your HTML, CSS and JS skills. 

You can find designs and all required assets in Figma (you may need to create a free account in order to be able to view project details such as colours, font sizes, grid size etc. )

Design link: https://www.figma.com/file/K8urLxuVcSezaoT3kzqmSH/FE-Developer-Test?node-id=0%3A1

## Notes
 - You can use any build process you feel comfortable with (plain HTML/CSS/JS files are fine too!)
 - We'd like to asses **YOUR** coding skills, so - if possible - please try not to rely on CSS frameworks such as Bootstrap 
 - The output doesn't have to be pixel perfect. (We will be looking at your problem solving skills and general approach to coding rather than measuring paddings between elements)
 - Please submit the work to a git repository and share the link with us
 - Ask as many questions as you like if you feel stuck

